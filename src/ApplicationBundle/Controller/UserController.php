<?php

    namespace ApplicationBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

    use ApplicationBundle\Form\UserType;
    use UserBundle\Entity\User;

    /**
     * User controller.
     *
     * @Route("/usuario")
     */
    class UserController extends Controller
    {
        /**
         * Lists all User entities.
         *
         * @Route("/", name="_user_index")
         * @Method("GET")
         * @Template()
         * @Security("has_role('ROLE_SUPER_ADMIN')")
         */
        public function indexAction()
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( "UserBundle:User" )->findAll();

            return [
                "entity" => $entity,
            ];
        }

        /**
         * Creates a new User entity.
         *
         * @Route("/", name="_user_create")
         * @Method("POST")
         * @Template("ApplicationBundle:User:new.html.twig")
         */
        public function createAction( Request $request )
        {
            $entity = new User();

            $form = $this->createCreateForm( $entity );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();

                $em->persist( $entity );
                $em->flush();

                return $this->redirect( $this->generateUrl( "_user_show", [ "id" => $entity->getId() ] ) );
            }

            return [
                "entity" => $entity,
                "form" => $form->createView(),
            ];
        }

        /**
         * Creates a form to create a User entity.
         *
         * @param User $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createCreateForm( User $entity )
        {
            $form = $this->createForm( new UserType(), $entity, [
                "action" => $this->generateUrl( "_user_create" ),
                "method" => "POST",
            ] )
            ;

            return $form;
        }

        /**
         * Displays a form to create a new User entity.
         *
         * @Route("/new", name="_user_new")
         * @Method("GET")
         * @Template()
         */
        public function newAction()
        {
            $entity = new User();
            $form = $this->createCreateForm( $entity );

            return [
                "entity" => $entity,
                "form" => $form->createView(),
            ];
        }

        /**
         * Finds and displays a User entity.
         *
         * @Route("/show/{id}", name="_user_show")
         * @Method("GET")
         * @Template()
         */
        public function showAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( "UserBundle:User" )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( "Unable to find User entity." );
            }

            $deleteForm = $this->createDeleteForm( $id );

            return [
                "entity" => $entity,
                "delete_form" => $deleteForm->createView(),
            ];
        }

        /**
         * Displays a form to edit an existing User entity.
         *
         * @Route("/edit/{id}", name="_user_edit")
         * @Method("GET")
         * @Template()
         */
        public function editAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( "UserBundle:User" )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( "Unable to find User entity." );
            }

            $editForm = $this->createEditForm( $entity );
            $deleteForm = $this->createDeleteForm( $id );

            return [
                "entity" => $entity,
                "edit_form" => $editForm->createView(),
                "delete_form" => $deleteForm->createView(),
            ];
        }

        /**
         * Creates a form to edit a User entity.
         *
         * @param User $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createEditForm( User $entity )
        {
            $form = $this->createForm( new UserType(), $entity, [
                "action" => $this->generateUrl( "_user_edit", [ "id" => $entity->getId() ] ),
                "method" => "PUT",
            ] )
            ;

            return $form;
        }

        /**
         * Edits an existing User entity.
         *
         * @Route("/update/{id}", name="_user_update")
         * @Method("PUT")
         * @Template("ApplicationBundle:User:edit.html.twig")
         */
        public function updateAction( Request $request, $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( "UserBundle:User" )->find( $id );
    
            if( !$entity )
            {
                throw $this->createNotFoundException( "Unable to find User entity." );
            }

            $deleteForm = $this->createDeleteForm( $id );
            $editForm = $this->createEditForm( $entity );
            $editForm->handleRequest( $request );

            if( $editForm->isValid() )
            {
                $em->flush();

                return $this->redirect( $this->generateUrl( "_user_edit", [ "id" => $id ] ) );
            }

            return [
                "entity" => $entity,
                "edit_form" => $editForm->createView(),
                "delete_form" => $deleteForm->createView(),
            ];
        }

        /**
         * Deletes a User entity.
         *
         * @Route("/delete/{id}", name="_user_delete")
         * @Method("DELETE")
         */
        public function deleteAction( Request $request, $id )
        {
            $form = $this->createDeleteForm( $id );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository( "UserBundle:User" )->find( $id );

                if( !$entity )
                {
                    throw $this->createNotFoundException( "Unable to find User entity." );
                }

                $em->remove( $entity );
                $em->flush();
            }

            return $this->redirect( $this->generateUrl( "usuario" ) );
        }

        /**
         * Creates a form to delete a User entity by id.
         *
         * @param mixed $id The entity id
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createDeleteForm( $id )
        {
            return $this->createFormBuilder()->setAction( $this->generateUrl( "_user_delete", [ "id" => $id ] ) )->setMethod( "DELETE" )->add( "submit", "submit", [ "label" => "Delete" ] )->getForm();
        }
    }