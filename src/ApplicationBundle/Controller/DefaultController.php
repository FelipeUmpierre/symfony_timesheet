<?php

    namespace ApplicationBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

    /**
     * @Route("/sis")
     */
    class DefaultController extends Controller
    {
        /**
         * @Route("/", name="_sis_default")
         * @Template()
         */
        public function indexAction()
        {
            return [
                "names" => [
                    "Felipe",
                    "Pieretti",
                    "Umpierre"
                ]
            ];
        }
    }