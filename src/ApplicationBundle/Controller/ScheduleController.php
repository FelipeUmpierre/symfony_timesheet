<?php

    namespace ApplicationBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Config\Definition\Exception\Exception;
    use Symfony\Component\HttpFoundation\Request;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

    use ApplicationBundle\Entity\Schedule;
    use ApplicationBundle\Form\ScheduleType;
    use UserBundle\Entity\User;

    /**
     * Schedule controller.
     *
     * @Route("/horario")
     */
    class ScheduleController extends Controller
    {
        /**
         * Lists all Schedule entities.
         *
         * @Route("/", name="_schedule_index")
         * @Template()
         */
        public function indexAction()
        {
            $em = $this->getDoctrine()->getManager();

            if( in_array( "ROLE_SUPER_ADMIN", $this->getUser()->getRoles() ) )
            {
                $schedules = $em->getRepository( "ApplicationBundle:Schedule" )->findSchedulesFromUsers();
            }
            else if( in_array( "ROLE_USER", $this->getUser()->getRoles() ) )
            {
                $schedules = $em->getRepository( "ApplicationBundle:Schedule" )->findSchedulesFromUserId( [
                        "users_id" => $this->getUser()->getId()
                    ] )
                ;
            }


            $users = $em->getRepository( "UserBundle:User" )->findAll();

            return [
                "schedules" => $schedules,
                "users" => $users,
            ];
        }

        /**
         * Creates a new Schedule entity.
         *
         * @Route("/search", name="_schedule_search")
         * @Method("GET")
         * @Template("ApplicationBundle:Schedule:index.html.twig")
         *
         * @param Request $request
         * @return array
         */
        public function searchAction( Request $request )
        {
            $params = $request->query->all();

            $em = $this->getDoctrine()->getManager();

            $schedules = $em->getRepository( "ApplicationBundle:Schedule" )->findSchedulesFromUserId( $params );
            $users = $em->getRepository( "UserBundle:User" )->findAll();

            return [
                "schedules" => $schedules,
                "users" => $users,
                "params" => $params,
            ];
        }

        /**
         * Creates a new Schedule entity.
         *
         * @Route("/create", name="_schedule_create")
         * @Method("POST")
         */
        public function createAction( Request $request )
        {
            $entity = new Schedule();

            $form = $this->createCreateForm( $entity );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                try
                {
                    $entity->setUsers( $em->getRepository( "UserBundle:User" )->find( $this->getUser()->getId() ) );

                    # fix the DateTime problem
                    $entity->setCreatedAt( date_create( $entity->getCreatedAt() ) );

                    $em->persist( $entity );
                    $em->flush();
                    $em->getConnection()->commit();
                }
                catch( Exception $e )
                {
                    $em->getConnection()->rollback();
                    throw $e;
                }

                return $this->redirect( $this->generateUrl( "_schedule_index" ) );
            }

            return [
                'entity' => $entity,
                'form' => $form->createView(),
            ];
        }

        /**
         * Creates a form to create a User entity.
         *
         * @param User $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createCreateForm( Schedule $entity )
        {
            $form = $this->createForm( new ScheduleType(), $entity, [
                "action" => $this->generateUrl( "_schedule_create" ),
                "method" => "POST",
            ] )
            ;

            return $form;
        }

        /**
         * Displays a form to create a new Schedule entity.
         *
         * @Route("/new", name="_schedule_new")
         * @Template()
         */
        public function newAction()
        {
            $entity = new Schedule();
            $form = $this->createCreateForm( $entity );

            return [
                "entity" => $entity,
                "form" => $form->createView(),
            ];
        }

        /**
         * Finds and displays a Schedule entity.
         *
         * @Route("/show/{id}", name="_schedule_show")
         * @Method("GET")
         * @Template()
         */
        public function showAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'ApplicationBundle:Schedule' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Schedule entity.' );
            }

            $deleteForm = $this->createDeleteForm( $id );

            return [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ];
        }

        /**
         * Displays a form to edit an existing Schedule entity.
         *
         * @Route("/edit/{id}", name="_schedule_edit")
         * @Template()
         */
        public function editAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'ApplicationBundle:Schedule' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Schedule entity.' );
            }

            $editForm = $this->createEditForm( $entity );
            $deleteForm = $this->createDeleteForm( $id );

            return [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ];
        }

        /**
         * Creates a form to edit a Schedule entity.
         *
         * @param Schedule $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createEditForm( Schedule $entity )
        {
            $form = $this->createForm( new ScheduleType(), $entity, [
                'action' => $this->generateUrl( '_schedule_edit', [ 'id' => $entity->getId() ] ),
                'method' => 'PUT',
            ] )
            ;

            return $form;
        }

        /**
         * Edits an existing Schedule entity.
         *
         * @Route("/update/{id}", name="_schedule_update")
         * @Method("PUT")
         * @Template("ApplicationBundle:Schedule:edit.html.twig")
         */
        public function updateAction( Request $request, $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'ApplicationBundle:Schedule' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Schedule entity.' );
            }

            $deleteForm = $this->createDeleteForm( $id );
            $editForm = $this->createEditForm( $entity );
            $editForm->handleRequest( $request );

            if( $editForm->isValid() )
            {
                $em->flush();

                return $this->redirect( $this->generateUrl( 'schedule_edit', [ 'id' => $id ] ) );
            }

            return [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ];
        }

        /**
         * Deletes a Schedule entity.
         *
         * @Route("/delete/{id}", name="_schedule_delete")
         * @Method("DELETE")
         */
        public function deleteAction( Request $request, $id )
        {
            $form = $this->createDeleteForm( $id );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository( 'ApplicationBundle:Schedule' )->find( $id );

                if( !$entity )
                {
                    throw $this->createNotFoundException( 'Unable to find Schedule entity.' );
                }

                $em->remove( $entity );
                $em->flush();
            }

            return $this->redirect( $this->generateUrl( 'schedule' ) );
        }

        /**
         * Creates a form to delete a Schedule entity by id.
         *
         * @param mixed $id The entity id
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createDeleteForm( $id )
        {
            return $this->createFormBuilder()->setAction( $this->generateUrl( 'schedule_delete', [ 'id' => $id ] ) )->setMethod( 'DELETE' )->add( 'submit', 'submit', [ 'label' => 'Delete' ] )->getForm();
        }

        /**
         * @Route("/now", name="_schedule_now")
         * @Method("GET")
         */
        public function nowAction()
        {
            $em = $this->getDoctrine()->getManager();

            $em->getConnection()->beginTransaction();

            try
            {
                $scheduleTypeToday = $em->getRepository( "ApplicationBundle:Schedule" )->findSchedulesFromUserId( [
                        "users_id" => $this->getUser()->getId(),
                        "exact_date" => date( "Y-m-d" )
                    ] )
                ;

                if( !empty( $scheduleTypeToday ) )
                {
                    foreach( $scheduleTypeToday as $key => $val )
                    {

                    }
                }

                $entity = new Schedule();
                $entity->setCreatedAt( date_create( $entity->getCreatedAt() ) );
                $entity->setUsers( $em->getRepository( "ApplicationBundle:User" )->find( $this->getUser()->getId() ) );
                # $entity->setScheduleType();

                $em->getConnection()->commit();
            }
            catch( Exception $e )
            {
                $em->getConnection()->rollback();
                throw $e;
            }
        }
    }