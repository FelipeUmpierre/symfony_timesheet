<?php

    namespace ApplicationBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class UserType extends AbstractType
    {
        /**
         * @param FormBuilderInterface $builder
         * @param array $options
         */
        public function buildForm( FormBuilderInterface $builder, array $options )
        {
            $builder->add( 'name', 'text', [
                    'label' => 'Nome',
                    'attr' => [
                        'class' => 'form-control',

                    ]
                ] )->add( 'email', 'text', [
                    'label' => 'E-mail',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ] )->add( 'password', 'password', [
                    'label' => 'Senha',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ] )->add( 'active', 'checkbox', [
                    'label' => 'Ativo',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ] );
        }

        /**
         * @param OptionsResolver $resolver
         */
        public function configureOptions( OptionsResolver $resolver )
        {
            $resolver->setDefaults( [
                'data_class' => 'ApplicationBundle\Entity\User'
            ] );
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'applicationbundle_user';
        }
    }
