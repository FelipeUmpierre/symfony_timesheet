<?php

    namespace ApplicationBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class ScheduleType extends AbstractType
    {
        /**
         * @param FormBuilderInterface $builder
         * @param array $options
         */
        public function buildForm( FormBuilderInterface $builder, array $options )
        {
            $builder->add( 'createdAt', 'text', [
                'attr' => [ 'class' => 'form-control' ]
            ] )->add( 'scheduleType' )
            ;
        }

        /**
         * @param OptionsResolver $resolver
         */
        public function configureOptions( OptionsResolver $resolver )
        {
            $resolver->setDefaults( [
                'data_class' => 'ApplicationBundle\Entity\Schedule'
            ] )
            ;
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'applicationbundle_schedule';
        }
    }
