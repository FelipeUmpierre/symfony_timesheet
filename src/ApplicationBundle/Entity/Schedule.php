<?php

    namespace ApplicationBundle\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;
    use UserBundle\Entity\User;

    /**
     * Schedule
     *
     * @ORM\Table(name="schedules")
     * @ORM\Entity(repositoryClass="ApplicationBundle\Entity\ScheduleRepository")
     * @ORM\HasLifecycleCallbacks
     */
    class Schedule
    {
        /**
         * @var integer
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="schedule")
         * @ORM\JoinColumn(name="users_id", referencedColumnName="id")
         */
        private $users;

        /**
         * @ORM\ManyToOne(targetEntity="ScheduleType", inversedBy="schedule")
         * @ORM\JoinColumn(name="schedules_type_id", referencedColumnName="id")
         */
        private $scheduleType;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="created_at", type="datetime")
         */
        private $createdAt;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="updated_at", type="datetime")
         */
        private $updatedAt;

        public function __construct()
        {
            $this->users = new ArrayCollection();
            $this->scheduleType = new ArrayCollection();
        }

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set createdAt
         *
         * @param \DateTime $createdAt
         * @return Time
         */
        public function setCreatedAt( $createdAt )
        {
            $this->createdAt = $createdAt;

            return $this;
        }

        /**
         * Get createdAt
         *
         * @return \DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Set updatedAt
         *
         * @param \DateTime $updatedAt
         * @return Time
         */
        public function setUpdatedAt( $updatedAt )
        {
            $this->updatedAt = $updatedAt;

            return $this;
        }

        /**
         *
         * @ORM\PrePersist
         * @ORM\PreUpdate
         */
        public function updatedTimestamps()
        {
            $this->setUpdatedAt( new \DateTime( "now" ) );

            if( $this->getCreatedAt() == null )
            {
                $this->setCreatedAt( new \DateTime( "now" ) );
            }
        }

        /**
         * Get updatedAt
         *
         * @return \DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * Set user
         *
         * @param User $users
         * @return \UserBundle\Entity\Schedule
         */
        public function setUsers( User $users )
        {
            $this->users = $users;

            return $this;
        }

        /**
         * Get user
         *
         * @return type
         */
        public function getUsers()
        {
            return $this->users;
        }

        /**
         * Set schedule type
         *
         * @param ScheduleType $scheduleType
         * @return \ApplicationBundle\Entity\Schedule
         */
        public function setScheduleType( ScheduleType $scheduleType )
        {
            $this->scheduleType = $scheduleType;

            return $this;
        }

        /**
         * Get schedule type
         *
         * @return type
         */
        public function getScheduleType()
        {
            return $this->scheduleType;
        }
   }   