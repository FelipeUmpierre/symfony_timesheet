<?php

    namespace ApplicationBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * ScheduleType
     *
     * @ORM\Table(name="schedules_type")
     * @ORM\Entity
     */
    class ScheduleType
    {
        /**
         * @var integer
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @var string
         *
         * @ORM\Column(name="name", type="string", length=255)
         */
        private $name;

        /**
         * @ORM\OneToMany(targetEntity="Schedule", mappedBy="scheduleType")
         */
        protected $schedule;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set name
         *
         * @param string $name
         * @return ScheduleType
         */
        public function setName( $name )
        {
            $this->name = $name;

            return $this;
        }

        /**
         * Get name
         *
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set schedule
         *
         * @param \ApplicationBundle\Entity\Schedule $schedule
         * @return User
         */
        public function setSchedule( Schedule $schedule = null )
        {
            $this->schedule = $schedule;

            return $this;
        }

        /**
         * Get schedule
         *
         * @return \ApplicationBundle\Entity\Schedule
         */
        public function getSchedule()
        {
            return $this->schedule;
        }

        public function __toString()
        {
            return $this->getName();
        }
    }