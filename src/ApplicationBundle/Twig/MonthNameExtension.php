<?php

    namespace ApplicationBundle\Twig;


    class MonthNameExtension extends \Twig_Extension
    {
        public function getFilters()
        {
            return [
                new \Twig_SimpleFilter( "monthName", [ $this, "monthNameFilter" ] ),
            ];
        }

        /**
         *
         *
         * @param $month
         * @return string
         */
        public function monthNameFilter( $month )
        {
            $months = [
                1 => "Janeiro",
                2 => "Fevereiro",
                3 => "Março",
                4 => "Abril",
                5 => "Maio",
                6 => "Junho",
                7 => "Julho",
                8 => "Agosto",
                9 => "Setembro",
                10 => "Outubro",
                11 => "Novembro",
                12 => "Dezembro"
            ];

            if( isset( $months[ $month ] ) )
            {
                return $months[ $month ];
            }

            return "---";
        }

        public function getName()
        {
            return "month_name_extension";
        }
    }