<?php

    namespace ApplicationBundle\Twig;

    class SumTimeExtension extends \Twig_Extension
    {
        public function getFilters()
        {
            return [
                new \Twig_SimpleFilter( "sumTime", [ $this, "sumTimeFilter" ] ),
            ];
        }

        public function sumTimeFilter( array $times )
        {
            $seconds = 0;

            foreach( $times as $time )
            {
                if( !$this->validateDate( $time, 'H:i:s' ) )
                {
                    break;
                }

                list( $hour, $minute, $second ) = explode( ":", $time );

                $seconds += $hour * 3600;
                $seconds += $minute * 60;
                $seconds += $second;
            }

            $hours = floor( $seconds / 3600 );
            $seconds -= $hours * 3600;
            $minutes = floor( $seconds / 60 );
            $seconds -= $minutes * 60;

            return sprintf( "%02d:%02d:%02d", $hours, $minutes, $seconds );
        }

        public function getName()
        {
            return "sum_time_extension";
        }

        private function validateDate( $dateTime, $format = "Y-m-d H:i:s" )
        {
            $d = \DateTime::createFromFormat( $format, $dateTime );

            return $d && $d->format( $format ) == $dateTime;
        }
    }