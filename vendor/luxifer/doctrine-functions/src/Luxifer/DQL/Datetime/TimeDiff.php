<?php

   namespace Luxifer\DQL\Datetime;

   use Doctrine\ORM\Query\AST\Functions\FunctionNode;
   use Doctrine\ORM\Query\Lexer;

   /**
    * TimeFunction ::= "TIMEDIFF" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
    * @author Felipe Pieretti Umpierre <umpierre.felipe@gmail.com>
    */
   class TimeDiff extends FunctionNode
   {
      public $firstDateTimeExpression = null;
      public $secondDateTimeExpression = null;

      public function parse( \Doctrine\ORM\Query\Parser $parser )
      {
         $parser->match( Lexer::T_IDENTIFIER );
         $parser->match( Lexer::T_OPEN_PARENTHESIS );
         $this->firstDateTimeExpression = $parser->ArithmeticPrimary();
         $parser->match( Lexer::T_COMMA );
         $this->secondDateTimeExpression = $parser->ArithmeticPrimary();
         $parser->match( Lexer::T_CLOSE_PARENTHESIS );
      }

      public function getSql( \Doctrine\ORM\Query\SqlWalker $sqlWalker )
      {
         return 'TIMEDIFF(' .
                 $this->firstDateTimeExpression->dispatch( $sqlWalker ) . ', ' .
                 $this->secondDateTimeExpression->dispatch( $sqlWalker ) .
                 ')';
      }
   }   