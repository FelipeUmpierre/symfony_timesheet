var loaderHTML = '<div class="text-center">' +
                      '<img src="' + $( "#modal").find( ".modal-loader" ).data( "loader" ) + '" alt="loading..." />' +
                  '</div>';

/* variável função para voltar ao padrão da modal */
var returnDefaultModal = function() {
    $( ".modal-body, .modal-footer" ).removeClass( "hide" );
    $( ".modal-loader" ).addClass( "hide" ).html( "" );
}

/* variável função para adicionar o loader na modal */
var loaderModal = function() {
    $( ".modal-body, .modal-footer" ).addClass( "hide" );
    $( ".modal-loader" ).removeClass( "hide" ).html( loaderHTML );
}

$( function() {
   
    //$( document ).on( "click", "#", function( e ) {
    //    e.preventDefault();
    //
    //    var $this = $( this );
    //
    //    if( confirm( "Deseja usar a hora atual?" ) )
    //    {
    //     $.post( DEFAULT + "horario/", function( e ) {
    //
    //     } );
    //    }
    //} );

    $( document ).on( "click", ".open-modal", function( e ) {

        e.preventDefault();

        var $this = $( this );
        var $data = $this.data();

        $.post( $data.action, function( e ) {
            $( "#modal" ).find( ".modal-body" ).html( e );
        } );

    } );

    /* Quando esconde a modal */
    $( "#modal" ).on( "hidden.bs.modal", function() {
        $( "#modal" ).find( ".modal-body" ).html( loaderHTML );
    } );
   
} );

function generateModal()
{

}