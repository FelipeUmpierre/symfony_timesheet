/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.24-log : Database - symfony
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`symfony` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `symfony`;

/*Table structure for table `schedules` */

DROP TABLE IF EXISTS `schedules`;

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `schedules_type_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_313BDC8E67B3B43D` (`users_id`),
  KEY `IDX_313BDC8E692AF56F` (`schedules_type_id`),
  CONSTRAINT `FK_313BDC8E692AF56F` FOREIGN KEY (`schedules_type_id`) REFERENCES `schedules_type` (`id`),
  CONSTRAINT `FK_313BDC8E67B3B43D` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `schedules` */

insert  into `schedules`(`id`,`users_id`,`schedules_type_id`,`created_at`,`updated_at`) values (1,1,1,'2015-02-18 10:00:00','2015-02-18 19:31:32'),(2,1,2,'2015-02-18 19:31:39','2015-02-18 19:31:39'),(3,2,1,'2015-02-18 18:31:45','2015-02-18 18:21:45'),(4,2,2,'2015-02-18 20:31:51','2015-02-18 20:21:51'),(5,1,1,'2015-02-25 08:55:41','2015-02-25 08:55:41'),(6,1,2,'2015-02-25 10:55:41','2015-02-25 10:55:41'),(7,1,1,'2015-02-17 09:00:00','2015-02-17 09:00:00'),(8,1,2,'2015-02-17 18:43:35','2015-02-17 18:43:35');

/*Table structure for table `schedules_type` */

DROP TABLE IF EXISTS `schedules_type`;

CREATE TABLE `schedules_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `schedules_type` */

insert  into `schedules_type`(`id`,`name`) values (1,'Entrada'),(2,'Saída');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`active`,`created_at`,`updated_at`) values (1,'Felipe','','',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Eduardo','','',0,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
