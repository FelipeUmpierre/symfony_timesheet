<?php

/* ApplicationBundle:Schedule:index.html.twig */
class __TwigTemplate_94ed6bc8947ee31b6c4cd37fdc56ae8d05cdc39c8b33f533029f09b28a013652 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("ApplicationBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ApplicationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Horários - ";
    }

    // line 4
    public function block_page_title($context, array $blocks = array())
    {
        echo "Horários";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "
\t";
        // line 8
        if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 9
            echo "        <div class=\"col-md-12\">
\t        <div class=\"portlet light bordered\">
\t            <div class=\"portlet-title\">
\t                <div class=\"caption\">
\t                    <span class=\"caption-subject uppercase font-blue-hoki\"><i class=\"fa fa-fw fa-search\"></i> pesquisar</span>
\t                </div>
\t            </div>
\t            <div class=\"portlet-body\">
\t                <form action=\"";
            // line 17
            echo $this->env->getExtension('routing')->getPath("_schedule_search");
            echo "\" method=\"get\">

\t                    <div class=\"row\">
\t                        <div class=\"col-md-3\">
\t                            <div class=\"form-group\">
\t                                <label>Usuários</label>
\t                                <select name=\"users_id\" class=\"form-control\">
\t                                    ";
            // line 24
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 25
                echo "\t                                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
                echo "\" ";
                echo ((($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "users_id", array(), "any", true, true) && ($this->getAttribute((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), "users_id", array()) == $this->getAttribute($context["user"], "id", array())))) ? ("selected") : (""));
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
                echo "</option>
\t                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "\t                                </select>
\t                            </div>
\t                        </div>
\t                        <div class=\"col-md-4\">
\t                            <div class=\"form-group\">
\t                                <label>Busca por Data</label>
\t                                <div class=\"row\">
\t                                    <div class=\"col-md-12\">
\t                                        <div class=\"row\">
\t                                            <div class=\"col-md-7\">
\t                                                <select name=\"month\" class=\"form-control\">
\t                                                    ";
            // line 38
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 12));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 39
                echo "\t                                                        <option value=\"";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('month_name_extension')->monthNameFilter($context["i"]), "html", null, true);
                echo "</option>
\t                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "\t                                                </select>
\t                                            </div>
\t                                            <div class=\"col-md-5\">
\t                                                <select name=\"year\" class=\"form-control\">
\t                                                    ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(twig_date_format_filter($this->env, "now", "Y"), 2015));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 46
                echo "\t                                                        <option value=\"";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</option>
\t                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "\t                                                </select>
\t                                            </div>
\t                                        </div>
\t                                    </div>
\t                                </div>
\t                            </div>
\t                        </div>
\t                    </div>

\t                    <hr />

\t                    <button type=\"submit\" class=\"btn blue-madison\"><i class=\"fa fa-fw fa-search\"></i> Pesquisar informações</button>
\t                    <a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("_schedule_index");
            echo "\" class=\"btn btn-link\">Limpar busca</a>

\t                </form>
\t            </div>
\t        </div>
\t    </div>
\t";
        }
        // line 67
        echo "
    <div class=\"col-md-12\">
        <div class=\"portlet light bordered\">
            <div class=\"portlet-title\">
                <div class=\"caption\">
                    <span class=\"caption-subject uppercase font-blue-dark\">lista dos horários</span>
                </div>

                <a href=\"#modal\" class=\"btn green-meadow pull-right margin-left-20 open-modal\" data-toggle=\"modal\" data-action=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("_schedule_new");
        echo "\">
                    <i class=\"fa fa-fw fa-clock-o\"></i> Adicionar meu horário
                </a>

                <a href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("_schedule_now");
        echo "\" class=\"btn grey-cararra pull-right\" id=\"add-in-schedule-actual-time\">
                    <i class=\"fa fa-fw fa-check-circle-o\"></i> Adicionar hora atual
                </a>
            </div>
            <div class=\"portlet-body\">

                ";
        // line 85
        if ((twig_length_filter($this->env, (isset($context["schedules"]) ? $context["schedules"] : $this->getContext($context, "schedules"))) > 0)) {
            // line 86
            echo "                    <table class=\"table table-striped table-bordered table-hover table-advance table-condensed\">
                        <thead>
                            <tr>
\t                            ";
            // line 89
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 90
                echo "                                    <th width=\"35%\"><i class=\"fa fa-fw fa-user\"></i> Usuário</th>
\t                            ";
            }
            // line 92
            echo "
                                <th class=\"text-center\" width=\"8%\"><i class=\"fa fa-fw fa-calendar-o\"></i> Dia</th>
                                <th class=\"text-center\" width=\"8%\"><i class=\"fa fa-fw fa-arrow-circle-down font-green-meadow\"></i> Entrada</th>
                                <th class=\"text-center\" width=\"8%\"><i class=\"fa fa-fw fa-arrow-circle-up font-red-intense\"></i> Saída</th>
                                <th class=\"text-center\" width=\"10%\"><i class=\"fa fa-fw fa-clock-o\"></i> Total do dia</th>

\t                            ";
            // line 98
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 99
                echo "\t                                <th class=\"text-center\" width=\"1%\"><!-- --></th>
                                ";
            }
            // line 101
            echo "                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 104
            $context["totalTimestamp"] = array();
            // line 105
            echo "
                            ";
            // line 106
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["schedules"]) ? $context["schedules"] : $this->getContext($context, "schedules")));
            foreach ($context['_seq'] as $context["_key"] => $context["schedule"]) {
                // line 107
                echo "                                ";
                $context["totalTimestamp"] = (( !(null === $this->getAttribute($context["schedule"], "difference", array()))) ? (twig_array_merge((isset($context["totalTimestamp"]) ? $context["totalTimestamp"] : $this->getContext($context, "totalTimestamp")), array(0 => twig_date_format_filter($this->env, $this->getAttribute($context["schedule"], "difference", array()), "H:i:s")))) : (twig_array_merge((isset($context["totalTimestamp"]) ? $context["totalTimestamp"] : $this->getContext($context, "totalTimestamp")), array(0 => 0))));
                // line 108
                echo "                                <tr>
\t                                ";
                // line 109
                if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                    // line 110
                    echo "                                        <td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["schedule"], 0, array()), "users", array()), "username", array()), "html", null, true);
                    echo "</td>
\t                                ";
                }
                // line 112
                echo "
                                    <td class=\"text-center\">";
                // line 113
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["schedule"], "d", array()), "d/m/Y"), "html", null, true);
                echo "</td>
                                    <td class=\"text-center\">";
                // line 114
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["schedule"], "start", array()), "H:i:s"), "html", null, true);
                echo "</td>
                                    <td class=\"text-center\">";
                // line 115
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["schedule"], "finish", array()), "H:i:s"), "html", null, true);
                echo "</td>
                                    <td class=\"text-center\">";
                // line 116
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["schedule"], "difference", array()), "H:i:s"), "html", null, true);
                echo "</td>

\t                                ";
                // line 118
                if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                    // line 119
                    echo "\t                                    <td class=\"text-center\">
\t                                        ";
                    // line 120
                    if (($this->getAttribute($context["schedule"], "difference", array()) >= "08:00:00")) {
                        // line 121
                        echo "\t                                            <i class=\"fa fa-fw fa-check-circle-o font-green-meadow\"></i>
\t                                        ";
                    } else {
                        // line 123
                        echo "\t                                            <i class=\"fa fa-fw fa-times-circle font-red-intense\"></i>
\t                                        ";
                    }
                    // line 125
                    echo "\t                                    </td>
\t                                ";
                }
                // line 127
                echo "                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schedule'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                            <tr>
                                <td colspan=\"";
            // line 130
            echo (($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) ? (4) : (3));
            echo "\" class=\"text-right\">Total do mês <i class=\"fa fa-fw fa-calendar-o\"></i></td>
                                <td class=\"text-center\">";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('sum_time_extension')->sumTimeFilter((isset($context["totalTimestamp"]) ? $context["totalTimestamp"] : $this->getContext($context, "totalTimestamp"))), "html", null, true);
            echo "</td>
\t                            ";
            // line 132
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 133
                echo "\t\t                            <td>";
                echo "</td>
\t                            ";
            }
            // line 135
            echo "                            </tr>
                        </tbody>
                    </table>
                ";
        } else {
            // line 139
            echo "                    <p class=\"font-grey-cascade\">Nenhum horário cadastrado. <a href=\"#modal\" class=\"open-modal\" data-toggle=\"modal\" data-action=\"";
            echo $this->env->getExtension('routing')->getPath("_schedule_new");
            echo "\">Clique aqui</a> para cadastrar um horário.</p>
                ";
        }
        // line 141
        echo "
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ApplicationBundle:Schedule:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  325 => 141,  319 => 139,  313 => 135,  308 => 133,  306 => 132,  302 => 131,  298 => 130,  295 => 129,  288 => 127,  284 => 125,  280 => 123,  276 => 121,  274 => 120,  271 => 119,  269 => 118,  264 => 116,  260 => 115,  256 => 114,  252 => 113,  249 => 112,  243 => 110,  241 => 109,  238 => 108,  235 => 107,  231 => 106,  228 => 105,  226 => 104,  221 => 101,  217 => 99,  215 => 98,  207 => 92,  203 => 90,  201 => 89,  196 => 86,  194 => 85,  185 => 79,  178 => 75,  168 => 67,  158 => 60,  144 => 48,  133 => 46,  129 => 45,  123 => 41,  112 => 39,  108 => 38,  95 => 27,  82 => 25,  78 => 24,  68 => 17,  58 => 9,  56 => 8,  53 => 7,  50 => 6,  44 => 4,  38 => 3,  11 => 1,);
    }
}
