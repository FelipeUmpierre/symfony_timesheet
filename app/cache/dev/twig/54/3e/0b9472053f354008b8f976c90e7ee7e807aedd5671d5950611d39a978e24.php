<?php

/* ApplicationBundle::layout.html.twig */
class __TwigTemplate_543e0b9472053f354008b8f976c90e7ee7e807aedd5671d5950611d39a978e24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]>
<html lang=\"en\" class=\"ie8 no-js\"> <![endif]-->
<!--[if IE 9]>
<html lang=\"en\" class=\"ie9 no-js\"> <![endif]-->
<!--[if !IE]><!-->
<html lang=\"en\" class=\"no-js\">
<!--<![endif]-->
<head>
\t<meta charset=\"utf-8\" />
\t<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo " Painel Administrativo</title>
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />
\t<meta content=\"\" name=\"description\" />
\t<meta content=\"\" name=\"author\" />

\t<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all\" rel=\"stylesheet\"
\t      type=\"text/css\" />
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/plugins/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"
\t      type=\"text/css\" />
\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/plugins/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"
\t      type=\"text/css\" />

\t";
        // line 24
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 36
        echo "
\t<link rel=\"shortcut icon\" href=\"favicon.ico\" />
</head>
<body class=\"page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo\">

<div class=\"page-header navbar navbar-fixed-top\">
\t<div class=\"page-header-inner\">

\t\t<div class=\"page-logo\">
\t\t\t<a href=\"index.html\">
\t\t\t\t<img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/layout4/img/logo-light.png"), "html", null, true);
        echo "\" alt=\"logo\" class=\"logo-default\" />
\t\t\t</a>
\t\t</div>

\t\t<div class=\"page-top\">
\t\t\t<div class=\"top-menu\">
\t\t\t\t<ul class=\"nav navbar-nav pull-right\">
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\" class=\"font-grey-gallery\">
\t\t\t\t\t\t\tDeslogar <i class=\"fa fa-fw fa-sign-out\"></i>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"clearfix\">
</div>

<div class=\"page-container\">

\t";
        // line 69
        $this->env->loadTemplate("ApplicationBundle::menu.html.twig")->display($context);
        // line 70
        echo "
\t<div class=\"page-content-wrapper\">
\t\t<div class=\"page-content\">
\t\t\t<div class=\"page-head\">
\t\t\t\t<div class=\"page-title\">
\t\t\t\t\t<h1>";
        // line 75
        $this->displayBlock('page_title', $context, $blocks);
        echo "</h1>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"row margin-top-10\">
\t\t\t\t";
        // line 80
        $this->displayBlock('content', $context, $blocks);
        // line 81
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"page-footer\">
\t<div class=\"page-footer-inner\">
\t\t";
        // line 88
        $this->displayBlock('footer', $context, $blocks);
        // line 89
        echo "\t</div>
\t<div class=\"scroll-to-top\">
\t\t<i class=\"icon-arrow-up\"></i>
\t</div>
</div>

<div id=\"modal\" class=\"modal fade\" tabindex=\"-1\" role=\"basic\" aria-hidden=\"true\">
\t<div class=\"modal-dialog\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<h4 class=\"modal-title\">Responsive</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t<img src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/leonardo.gif"), "html", null, true);
        echo "\" alt=\"loading...\" />
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"modal-loader\" data-loader=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/leonardo.gif"), "html", null, true);
        echo "\"></div>
\t\t</div>
\t</div>
</div>

<!--[if lt IE 9]>
";
        // line 113
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "5ab99ab_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5ab99ab_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/5ab99ab_respond.min_1.js");
            // line 116
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
";
            // asset "5ab99ab_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5ab99ab_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/5ab99ab_excanvas.min_2.js");
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "5ab99ab"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5ab99ab") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/5ab99ab.js");
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
        // line 118
        echo "<![endif]-->

<script>var DEFAULT = \"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo "/\";</script>
\t";
        // line 121
        $this->displayBlock('javascript', $context, $blocks);
        // line 138
        echo "
<script>
\tjQuery( document ).ready( function()
\t{
\t\tMetronic.init();
\t} );
</script>
</body>
</html>";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
    }

    // line 24
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 25
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "33f0874_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_bootstrap.min_1.css");
            // line 33
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_bootstrap-switch.min_2.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_components_3.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_3") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_plugins_4.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_4") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_layout_5.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_5") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_light_6.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
            // asset "33f0874_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874_6") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874_custom_7.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
        } else {
            // asset "33f0874"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_33f0874") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/33f0874.css");
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
        }
        unset($context["asset_url"]);
        // line 35
        echo "\t";
    }

    // line 75
    public function block_page_title($context, array $blocks = array())
    {
        echo "Dashboard";
    }

    // line 80
    public function block_content($context, array $blocks = array())
    {
    }

    // line 88
    public function block_footer($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " Todos os direitos reservados.";
    }

    // line 121
    public function block_javascript($context, array $blocks = array())
    {
        // line 122
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4b2dea0_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery.min_1.js");
            // line 135
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery-migrate.min_2.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery-ui-1.10.3.custom.min_3.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_3") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_bootstrap.min_4.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_4") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_bootstrap-hover-dropdown.min_5.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_5") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery.slimscroll.min_6.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_6") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery.blockui.min_7.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_7") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery.cokie.min_8.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_8") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_jquery.uniform.min_9.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_9") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_bootstrap-switch.min_10.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_10"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_10") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_metronic_11.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "4b2dea0_11"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0_11") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0_main_12.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "4b2dea0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b2dea0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4b2dea0.js");
            echo "\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 137
        echo "\t";
    }

    public function getTemplateName()
    {
        return "ApplicationBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  383 => 137,  303 => 135,  298 => 122,  295 => 121,  288 => 88,  283 => 80,  277 => 75,  273 => 35,  223 => 33,  218 => 25,  215 => 24,  210 => 11,  198 => 138,  196 => 121,  192 => 120,  188 => 118,  168 => 116,  164 => 113,  155 => 107,  148 => 103,  132 => 89,  130 => 88,  121 => 81,  119 => 80,  111 => 75,  104 => 70,  102 => 69,  84 => 54,  73 => 46,  61 => 36,  59 => 24,  53 => 21,  48 => 19,  37 => 11,  25 => 1,);
    }
}
