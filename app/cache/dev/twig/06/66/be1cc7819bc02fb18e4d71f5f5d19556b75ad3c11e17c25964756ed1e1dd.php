<?php

/* UserBundle:Security:login.html.twig */
class __TwigTemplate_0666be1cc7819bc02fb18e4d71f5f5d19556b75ad3c11e17c25964756ed1e1dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("UserBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<form action=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

\t\t<div class=\"form-title\">
\t\t\t<span class=\"form-title\">Bem vindo.</span>
\t\t\t<span class=\"form-subtitle\">Faça o login para acessar o sistema.</span>
\t\t</div>

\t\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 13
            echo "\t\t\t<div class=\"alert alert-danger\">
\t\t\t\t";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 15
                echo "\t\t\t\t\t<span>
\t\t\t\t\t\t";
                // line 16
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
\t\t\t\t\t</span>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "\t\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
\t\t<div class=\"form-group\">
\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Login</label>
\t\t\t<input class=\"form-control form-control-solid placeholder-no-fix\" type=\"text\" autocomplete=\"off\" placeholder=\"Login\" id=\"username\" name=\"_username\" />
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"control-label visible-ie8 visible-ie9\">Senha</label>
\t\t\t<input class=\"form-control form-control-solid placeholder-no-fix\" type=\"password\" autocomplete=\"off\" placeholder=\"Senha\" id=\"password\" name=\"_password\" />
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label for=\"remember_me\" class=\"rememberme check\">
\t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" /> Lembrar do login
\t\t\t</label>
\t\t</div>

\t\t<div class=\"form-actions\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-block uppercase\">Login</button>
\t\t</div>
\t</form>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 21,  77 => 19,  68 => 16,  65 => 15,  61 => 14,  58 => 13,  54 => 12,  44 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
