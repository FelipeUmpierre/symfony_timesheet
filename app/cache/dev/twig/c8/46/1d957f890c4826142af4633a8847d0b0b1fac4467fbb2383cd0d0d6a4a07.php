<?php

/* ApplicationBundle::menu.html.twig */
class __TwigTemplate_c8461d957f890c4826142af4633a8847d0b0b1fac4467fbb2383cd0d0d6a4a07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-sidebar-wrapper\">
\t<div class=\"page-sidebar navbar-collapse collapse\">
\t\t<ul class=\"page-sidebar-menu \" data-keep-expanded=\"false\" data-auto-scroll=\"true\" data-slide-speed=\"200\">
\t\t\t";
        // line 4
        if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 5
            echo "\t\t\t\t<li class=\"";
            echo ((preg_match("/_user_(.*)/", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method"))) ? ("active") : (""));
            echo "\">
\t\t\t\t\t<a href=\"";
            // line 6
            echo $this->env->getExtension('routing')->getPath("_user_index");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-fw fa-users\"></i>
\t\t\t\t\t\t<span class=\"title\">Usuários</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
        } elseif ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 12
            echo "\t\t\t\t<li class=\"";
            echo ((preg_match("/_user_(.*)/", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method"))) ? ("active") : (""));
            echo "\">
\t\t\t\t\t<a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_schedule_edit", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "security", array()), "getToken", array(), "method"), "getUser", array(), "method"), "getId", array(), "method"))), "html", null, true);
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-fw fa-user-md\"></i>
\t\t\t\t\t\t<span class=\"title\">Meus dados</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
        }
        // line 19
        echo "\t\t\t<li class=\"";
        echo ((preg_match("/_schedule_(.*)/", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method"))) ? ("active") : (""));
        echo "\">
\t\t\t\t<a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("_schedule_index");
        echo "\">
\t\t\t\t\t<i class=\"fa fa-fw fa-clock-o\"></i>
\t\t\t\t\t<span class=\"title\">Horário</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t</ul>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "ApplicationBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 20,  54 => 19,  45 => 13,  40 => 12,  31 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
