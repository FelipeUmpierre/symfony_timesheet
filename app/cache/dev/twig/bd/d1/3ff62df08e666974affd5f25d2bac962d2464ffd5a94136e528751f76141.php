<?php

/* UserBundle::layout.html.twig */
class __TwigTemplate_bdd13ff62df08e666974affd5f25d2bac962d2464ffd5a94136e528751f76141 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]><html lang=\"en\" class=\"ie8 no-js\"> <![endif]-->
<!--[if IE 9]><html lang=\"en\" class=\"ie9 no-js\"> <![endif]-->
<!--[if !IE]><!-->
<html lang=\"en\">
<!--<![endif]-->
<head>
    <meta charset=\"utf-8\"/>
    <title>Metronic | Login Form 2</title>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\"/>
    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">
    <meta content=\"\" name=\"description\"/>
    <meta content=\"\" name=\"author\"/>

    <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all\" rel=\"stylesheet\" type=\"text/css\" />
\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/plugins/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"
\t      type=\"text/css\" />
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/plugins/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"
\t      type=\"text/css\" />

    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/plugins/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>


    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/pages/css/login2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>

    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/css/components.css"), "html", null, true);
        echo "\" id=\"style_components\" rel=\"stylesheet\" type=\"text/css\"/>
    <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/global/css/plugins.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
    <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/layout/css/layout.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/layout/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" id=\"style_color\"/>
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/layout/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>

    <link rel=\"shortcut icon\" href=\"favicon.ico\"/>
</head>

<body class=\"login\">

    <div class=\"menu-toggler sidebar-toggler\">
    </div>

    <div class=\"logo\">
        <a href=\"index.html\">
            <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets/admin/layout3/img/logo-big-white.png"), "html", null, true);
        echo "\" style=\"height: 17px;\" alt=\"\"/>
        </a>
    </div>

    <div class=\"content\">


        ";
        // line 50
        $this->displayBlock('body', $context, $blocks);
        // line 53
        echo "    </div>
    <div class=\"copyright hide\">
        2014 © Metronic. Admin Dashboard Template.
    </div>

    <script>var DEFAULT = \"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo "/\";</script>
    ";
        // line 59
        $this->displayBlock('javascript', $context, $blocks);
        // line 70
        echo "
    <script>
\t    jQuery( document ).ready( function()
\t    {
\t\t    Metronic.init();
\t    } );
    </script>
</body>
</html>




";
    }

    // line 50
    public function block_body($context, array $blocks = array())
    {
        // line 51
        echo "            ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 52
        echo "        ";
    }

    // line 51
    public function block_fos_user_content($context, array $blocks = array())
    {
    }

    // line 59
    public function block_javascript($context, array $blocks = array())
    {
        // line 60
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "ec09dfc_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_jquery.min_1.js");
            // line 67
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "ec09dfc_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_jquery-migrate.min_2.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "ec09dfc_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_jquery-ui-1.10.3.custom.min_3.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "ec09dfc_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_3") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_bootstrap.min_4.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "ec09dfc_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_4") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_jquery.uniform.min_5.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "ec09dfc_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc_5") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc_metronic_6.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "ec09dfc"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ec09dfc") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/ec09dfc.js");
            echo "\t    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 69
        echo "    ";
    }

    public function getTemplateName()
    {
        return "UserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 69,  158 => 67,  153 => 60,  150 => 59,  145 => 51,  141 => 52,  138 => 51,  135 => 50,  118 => 70,  116 => 59,  112 => 58,  105 => 53,  103 => 50,  93 => 43,  78 => 31,  74 => 30,  70 => 29,  66 => 28,  62 => 27,  57 => 25,  51 => 22,  45 => 19,  40 => 17,  22 => 1,);
    }
}
