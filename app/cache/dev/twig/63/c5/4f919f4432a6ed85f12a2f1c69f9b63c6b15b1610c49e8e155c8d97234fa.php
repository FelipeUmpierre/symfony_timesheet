<?php

/* ApplicationBundle:User:index.html.twig */
class __TwigTemplate_63c54f919f4432a6ed85f12a2f1c69f9b63c6b15b1610c49e8e155c8d97234fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("ApplicationBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ApplicationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Usuários - ";
    }

    // line 4
    public function block_page_title($context, array $blocks = array())
    {
        echo "Usuários";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"col-md-12\">
      <div class=\"portlet light bordered\">
         <div class=\"portlet-title\">
            <div class=\"caption\">
               <span class=\"caption-subject uppercase font-blue-hoki\"><i class=\"fa fa-fw fa-search\"></i> pesquisar</span>
            </div>
         </div>
         <div class=\"portlet-body\">

         </div>
      </div>
   </div>

   <div class=\"col-md-12\">
      <div class=\"portlet light bordered\">
         <div class=\"portlet-title\">
            <div class=\"caption\">
               <span class=\"caption-subject uppercase font-blue-dark\">lista dos horários</span>
            </div>
            
            <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("_user_new");
        echo "\" class=\"btn green-meadow pull-right\">
               <i class=\"fa fa-fw fa-plus-square\"></i> Adicionar novo usuário
            </a>
         </div>
         <div class=\"portlet-body\">

            ";
        // line 33
        if ((twig_length_filter($this->env, (isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity"))) > 0)) {
            // line 34
            echo "               
               <table class=\"table table-striped table-bordered table-hover table-advance table-condensed\">
                  <thead>
                     <tr>
                        <th class=\"text-center\" width=\"3%\">#</th>
                        <th class=\"text-left\">Nome</th>
                        <th class=\"text-left\">E-mail</th>
                        <th class=\"text-center\" width=\"10%\">Criado em</th>
                        <th class=\"text-center\" width=\"10%\"><!-- --></th>
                     </tr>
                  </thead>
                  <tbody>
                     ";
            // line 46
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")));
            foreach ($context['_seq'] as $context["_key"] => $context["entities"]) {
                // line 47
                echo "                        <tr>
                           <td class=\"text-center\">";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["entities"], "id", array()), "html", null, true);
                echo "</td>
                           <td>";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["entities"], "username", array()), "html", null, true);
                echo "</td>
                           <td>";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["entities"], "email", array()), "html", null, true);
                echo "</td>
                           <td class=\"text-center\">";
                // line 51
                if ($this->getAttribute($context["entities"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entities"], "createdAt", array()), "d/m/Y"), "html", null, true);
                }
                echo "</td>
                           <td class=\"text-center\">
                              <a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_user_edit", array("id" => $this->getAttribute($context["entities"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs blue-dark\"><i class=\"fa fa-fw fa-pencil\"></i></a>
                              <a href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_user_delete", array("id" => $this->getAttribute($context["entities"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs red-flamingo\"><i class=\"fa fa-fw fa-times\"></i></a>
                           </td>
                        </tr>
                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entities'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "                  </tbody>
               </table>

            ";
        } else {
            // line 62
            echo "               <p class=\"font-grey-cascade\">Nenhum usuário cadastrado. <a href=\"";
            echo $this->env->getExtension('routing')->getPath("_user_new");
            echo "\">Clique aqui</a> para cadastrar um horário.</p>
            ";
        }
        // line 64
        echo "
         </div>
      </div>
   </div>

";
    }

    public function getTemplateName()
    {
        return "ApplicationBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 64,  146 => 62,  140 => 58,  130 => 54,  126 => 53,  119 => 51,  115 => 50,  111 => 49,  107 => 48,  104 => 47,  100 => 46,  86 => 34,  84 => 33,  75 => 27,  53 => 7,  50 => 6,  44 => 4,  38 => 3,  11 => 1,);
    }
}
