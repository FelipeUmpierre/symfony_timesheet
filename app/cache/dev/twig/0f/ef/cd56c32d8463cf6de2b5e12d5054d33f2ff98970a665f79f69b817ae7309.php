<?php

/* ApplicationBundle:User:new.html.twig */
class __TwigTemplate_0fefcd56c32d8463cf6de2b5e12d5054d33f2ff98970a665f79f69b817ae7309 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("ApplicationBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ApplicationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Usuários - ";
    }

    // line 4
    public function block_page_title($context, array $blocks = array())
    {
        echo "Adicionando Usuário";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "
   <div class=\"col-md-12\">
      <div class=\"portlet light bordered\">
         <div class=\"portlet-title\">
            <div class=\"caption\">
               <span class=\"caption-subject uppercase font-blue-chambray\"><i class=\"fa fa-fw fa-user\"></i> Adicionando usuário</span>
            </div>
         </div>
         <div class=\"portlet-body\">
            ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
            
               <div class=\"row\">
                  <div class=\"col-md-6\">
                     <div class=\"form-group\">
                        ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label');
        echo "
                        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                     </div>

                     <div class=\"form-group\">
                        ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "
                        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                     </div>

                     <div class=\"form-group\">
                        ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'label');
        echo "
                        ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'widget');
        echo "
                     </div>

                     <div class=\"form-group\">
                        ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "active", array()), 'label');
        echo "
                        ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "active", array()), 'widget');
        echo "
                     </div>
                  </div>
               </div>
                     
               <hr />

               <button type=\"submit\" class=\"btn blue-hoki\"><i class=\"fa fa-fw fa-check-circle-o\"></i> Salvar</button>
               <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("_user_index");
        echo "\" class=\"btn bnt-link\">Cancelar</a>
            
            ";
        // line 47
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
         </div>
      </div>
   </div>

";
    }

    public function getTemplateName()
    {
        return "ApplicationBundle:User:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 47,  120 => 45,  109 => 37,  105 => 36,  98 => 32,  94 => 31,  87 => 27,  83 => 26,  76 => 22,  72 => 21,  64 => 16,  53 => 7,  50 => 6,  44 => 4,  38 => 3,  11 => 1,);
    }
}
