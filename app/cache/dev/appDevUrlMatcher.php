<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/js/5ab99ab')) {
            // _assetic_5ab99ab
            if ($pathinfo === '/js/5ab99ab.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '5ab99ab',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_5ab99ab',);
            }

            if (0 === strpos($pathinfo, '/js/5ab99ab_')) {
                // _assetic_5ab99ab_0
                if ($pathinfo === '/js/5ab99ab_respond.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5ab99ab',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_5ab99ab_0',);
                }

                // _assetic_5ab99ab_1
                if ($pathinfo === '/js/5ab99ab_excanvas.min_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5ab99ab',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_5ab99ab_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css/33f0874')) {
            // _assetic_33f0874
            if ($pathinfo === '/css/33f0874.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_33f0874',);
            }

            if (0 === strpos($pathinfo, '/css/33f0874_')) {
                if (0 === strpos($pathinfo, '/css/33f0874_bootstrap')) {
                    // _assetic_33f0874_0
                    if ($pathinfo === '/css/33f0874_bootstrap.min_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_33f0874_0',);
                    }

                    // _assetic_33f0874_1
                    if ($pathinfo === '/css/33f0874_bootstrap-switch.min_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_33f0874_1',);
                    }

                }

                // _assetic_33f0874_2
                if ($pathinfo === '/css/33f0874_components_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_33f0874_2',);
                }

                // _assetic_33f0874_3
                if ($pathinfo === '/css/33f0874_plugins_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_33f0874_3',);
                }

                if (0 === strpos($pathinfo, '/css/33f0874_l')) {
                    // _assetic_33f0874_4
                    if ($pathinfo === '/css/33f0874_layout_5.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_33f0874_4',);
                    }

                    // _assetic_33f0874_5
                    if ($pathinfo === '/css/33f0874_light_6.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_33f0874_5',);
                    }

                }

                // _assetic_33f0874_6
                if ($pathinfo === '/css/33f0874_custom_7.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '33f0874',  'pos' => 6,  '_format' => 'css',  '_route' => '_assetic_33f0874_6',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js')) {
            if (0 === strpos($pathinfo, '/js/4b2dea0')) {
                // _assetic_4b2dea0
                if ($pathinfo === '/js/4b2dea0.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_4b2dea0',);
                }

                if (0 === strpos($pathinfo, '/js/4b2dea0_')) {
                    if (0 === strpos($pathinfo, '/js/4b2dea0_jquery')) {
                        // _assetic_4b2dea0_0
                        if ($pathinfo === '/js/4b2dea0_jquery.min_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_0',);
                        }

                        if (0 === strpos($pathinfo, '/js/4b2dea0_jquery-')) {
                            // _assetic_4b2dea0_1
                            if ($pathinfo === '/js/4b2dea0_jquery-migrate.min_2.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_1',);
                            }

                            // _assetic_4b2dea0_2
                            if ($pathinfo === '/js/4b2dea0_jquery-ui-1.10.3.custom.min_3.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_2',);
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/js/4b2dea0_bootstrap')) {
                        // _assetic_4b2dea0_3
                        if ($pathinfo === '/js/4b2dea0_bootstrap.min_4.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_3',);
                        }

                        // _assetic_4b2dea0_4
                        if ($pathinfo === '/js/4b2dea0_bootstrap-hover-dropdown.min_5.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_4',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/js/4b2dea0_jquery.')) {
                        // _assetic_4b2dea0_5
                        if ($pathinfo === '/js/4b2dea0_jquery.slimscroll.min_6.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_5',);
                        }

                        // _assetic_4b2dea0_6
                        if ($pathinfo === '/js/4b2dea0_jquery.blockui.min_7.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_6',);
                        }

                        // _assetic_4b2dea0_7
                        if ($pathinfo === '/js/4b2dea0_jquery.cokie.min_8.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_7',);
                        }

                        // _assetic_4b2dea0_8
                        if ($pathinfo === '/js/4b2dea0_jquery.uniform.min_9.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_8',);
                        }

                    }

                    // _assetic_4b2dea0_9
                    if ($pathinfo === '/js/4b2dea0_bootstrap-switch.min_10.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_9',);
                    }

                    if (0 === strpos($pathinfo, '/js/4b2dea0_m')) {
                        // _assetic_4b2dea0_10
                        if ($pathinfo === '/js/4b2dea0_metronic_11.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_10',);
                        }

                        // _assetic_4b2dea0_11
                        if ($pathinfo === '/js/4b2dea0_main_12.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4b2dea0',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_4b2dea0_11',);
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/fe3ac8c')) {
                // _assetic_fe3ac8c
                if ($pathinfo === '/js/fe3ac8c.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'fe3ac8c',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_fe3ac8c',);
                }

                // _assetic_fe3ac8c_0
                if ($pathinfo === '/js/fe3ac8c_main_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'fe3ac8c',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_fe3ac8c_0',);
                }

            }

            if (0 === strpos($pathinfo, '/js/ec09dfc')) {
                // _assetic_ec09dfc
                if ($pathinfo === '/js/ec09dfc.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_ec09dfc',);
                }

                if (0 === strpos($pathinfo, '/js/ec09dfc_')) {
                    if (0 === strpos($pathinfo, '/js/ec09dfc_jquery')) {
                        // _assetic_ec09dfc_0
                        if ($pathinfo === '/js/ec09dfc_jquery.min_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_0',);
                        }

                        if (0 === strpos($pathinfo, '/js/ec09dfc_jquery-')) {
                            // _assetic_ec09dfc_1
                            if ($pathinfo === '/js/ec09dfc_jquery-migrate.min_2.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_1',);
                            }

                            // _assetic_ec09dfc_2
                            if ($pathinfo === '/js/ec09dfc_jquery-ui-1.10.3.custom.min_3.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_2',);
                            }

                        }

                    }

                    // _assetic_ec09dfc_3
                    if ($pathinfo === '/js/ec09dfc_bootstrap.min_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_3',);
                    }

                    // _assetic_ec09dfc_4
                    if ($pathinfo === '/js/ec09dfc_jquery.uniform.min_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_4',);
                    }

                    // _assetic_ec09dfc_5
                    if ($pathinfo === '/js/ec09dfc_metronic_6.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec09dfc',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_ec09dfc_5',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/sis')) {
            // _sis_default
            if (rtrim($pathinfo, '/') === '/sis/sis') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_sis_default');
                }

                return array (  '_controller' => 'ApplicationBundle\\Controller\\DefaultController::indexAction',  '_route' => '_sis_default',);
            }

            if (0 === strpos($pathinfo, '/sis/horario')) {
                // _schedule_index
                if (rtrim($pathinfo, '/') === '/sis/horario') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_schedule_index');
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::indexAction',  '_route' => '_schedule_index',);
                }

                // _schedule_search
                if ($pathinfo === '/sis/horario/search') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__schedule_search;
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::searchAction',  '_route' => '_schedule_search',);
                }
                not__schedule_search:

                // _schedule_create
                if ($pathinfo === '/sis/horario/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not__schedule_create;
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::createAction',  '_route' => '_schedule_create',);
                }
                not__schedule_create:

                // _schedule_new
                if ($pathinfo === '/sis/horario/new') {
                    return array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::newAction',  '_route' => '_schedule_new',);
                }

                // _schedule_show
                if (0 === strpos($pathinfo, '/sis/horario/show') && preg_match('#^/sis/horario/show/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__schedule_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_schedule_show')), array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::showAction',));
                }
                not__schedule_show:

                // _schedule_edit
                if (0 === strpos($pathinfo, '/sis/horario/edit') && preg_match('#^/sis/horario/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_schedule_edit')), array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::editAction',));
                }

                // _schedule_update
                if (0 === strpos($pathinfo, '/sis/horario/update') && preg_match('#^/sis/horario/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not__schedule_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_schedule_update')), array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::updateAction',));
                }
                not__schedule_update:

                // _schedule_delete
                if (0 === strpos($pathinfo, '/sis/horario/delete') && preg_match('#^/sis/horario/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not__schedule_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_schedule_delete')), array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::deleteAction',));
                }
                not__schedule_delete:

                // _schedule_now
                if ($pathinfo === '/sis/horario/now') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__schedule_now;
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\ScheduleController::nowAction',  '_route' => '_schedule_now',);
                }
                not__schedule_now:

            }

            if (0 === strpos($pathinfo, '/sis/usuario')) {
                // _user_index
                if (rtrim($pathinfo, '/') === '/sis/usuario') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__user_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_user_index');
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::indexAction',  '_route' => '_user_index',);
                }
                not__user_index:

                // _user_create
                if ($pathinfo === '/sis/usuario/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not__user_create;
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::createAction',  '_route' => '_user_create',);
                }
                not__user_create:

                // _user_new
                if ($pathinfo === '/sis/usuario/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__user_new;
                    }

                    return array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::newAction',  '_route' => '_user_new',);
                }
                not__user_new:

                // _user_show
                if (0 === strpos($pathinfo, '/sis/usuario/show') && preg_match('#^/sis/usuario/show/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__user_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_user_show')), array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::showAction',));
                }
                not__user_show:

                // _user_edit
                if (0 === strpos($pathinfo, '/sis/usuario/edit') && preg_match('#^/sis/usuario/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not__user_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_user_edit')), array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::editAction',));
                }
                not__user_edit:

                // _user_update
                if (0 === strpos($pathinfo, '/sis/usuario/update') && preg_match('#^/sis/usuario/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not__user_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_user_update')), array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::updateAction',));
                }
                not__user_update:

                // _user_delete
                if (0 === strpos($pathinfo, '/sis/usuario/delete') && preg_match('#^/sis/usuario/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not__user_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_user_delete')), array (  '_controller' => 'ApplicationBundle\\Controller\\UserController::deleteAction',));
                }
                not__user_delete:

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // root
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'root');
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  'route' => 'fos_user_security_login',  'permantent' => true,  '_route' => 'root',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
